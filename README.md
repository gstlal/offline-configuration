# Offline Configuration

## Downloading files:
### The following files can be downloaded from the DCC at `https://dcc.ligo.org/LIGO-T2300120`:
- MANIFOLD mass model in hdf5
- MANIFOLD template bank (Edward) in hdf5
- MANIFOLD template bank (Edward) in xml
- MANIFOLD template bank (Jacob) in hdf5
- MANIFOLD template bank (Jacob) in xml
- MANIFOLD template bank in hdf5
- MANIFOLD template bank in xml
- mass model for FGMC p-astro method
- mass model for empirical/data-driven p-astro methof
- (for reference) O3 dtdphi file for imbh sub bank
- (for reference) O3 dtdphi file for non-imbh sub bank
- (for reference) O3 iDQ file
- (for reference) O3 mass model
- (for reference) bns-small template bank
- (for reference) bns-small mass model

To download these on a cluster, authenticate using the following command:
```python
kinit albert.einstein@LIGO.ORG
```
Then run:

```python
source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh
conda activate igwn
dcc archive --archive-dir=. --files -i T2300120-v1
conda deactivate
```

### The following files can be downloaded from git:
- O3 sub banks and template bank

To download these, run the following command:
```python
git clone git@git.ligo.org:gstlal/o3a.git
```

## Run an offline analysis
1. Create directories (bank/, mass_model/, idq/ dtdphi/) in the working directory
2. Download the banks, mass models, idq files, and dtdphi files, and put them in relevant directories created in the step above.
3. Edit the contents of config.yml

